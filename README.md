# Clear_dust

This is a small program designed to quickly clear the cache and remove old packages that are idle in the OS.

### ![\[ Man \]](cleard_man)
>```
> - Have possibility to see disk size and how much memory is used
> - Clear cache Browser
>```

---

><b>Note:</b>
>1. Run the command to make it executable:
>```
>chmod +x script.sh
>```
>2. After that you can run it just like
>```
>  ./script.sh
> ```

>And also I want to apologize for so many commit, when I first started working with git, I commited one file at a time, not in packages, thanks for your understanding.
